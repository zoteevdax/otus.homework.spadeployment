import { useEffect, useState } from "react";
import "./App.css";

interface WeatherForecast {
  date: string;
  temperatureC: number;
  temperatureF: number;
  summary: string;
}

function App() {
  const [weatherData, setWeatherData] = useState<WeatherForecast[]>([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetch("http://localhost:5247/WeatherForecast")
      .then((response) => response.json())
      .then((data) => {
        setWeatherData(data);
        setLoading(false);
      });
  }, []);

  if (loading) {
    return <div className="loader">загрузка данных...</div>;
  }

  return (
    <div className="App">
      <h1>Прогноз погоды на 5 дней</h1>
      <div className="weather-forecast">
        {weatherData.map((item) => (
          <div className="forecast-block">
            <div className="forecast-header">{item.date}</div>

            <div className="forecast-content">
              <div className="forecast-temperature">
                <div>Temperature (C):</div>
                <div>{item.temperatureC}</div>
              </div>

              <div className="forecast-temperature">
                <div>Temperature (F):</div>
                <div>{item.temperatureF}</div>
              </div>

              <div>{item.summary}</div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default App;
